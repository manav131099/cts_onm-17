rm(list = ls())
TIMESTAMPSALARM = NULL
TIMESTAMPSALARM2 = NULL
METERCALLED = 0
ltcutoff = .030
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
secondGenData = function(filepath,writefilepath)
{
  ratdiv = 0
  {
		if(METERCALLED == 1){
	 		TIMESTAMPSALARM <<- NULL
			ratdiv = 291.5
			}
		else if(METERCALLED == 2){
		  ratdiv = 218.625
			TIMESTAMPSALARM2 <<- NULL}
	}
  dataread = read.table(filepath,header = T,sep = "\t")
	dataread2 = dataread
	dataread = dataread2[complete.cases(dataread2[,2]),]
  LR=as.numeric(dataread[nrow(dataread),2])
  LT=dataread[nrow(dataread),1]
  Eac2 = round(as.numeric(dataread[nrow(dataread),2]) - as.numeric(dataread[1,2]),1)
	dataread = dataread2[complete.cases(dataread2[,9]),]
	Eac1 = format(round(sum(as.numeric(dataread[,9]))/12,1),nsmall=1)
	dspy = round(as.numeric(Eac1)/ratdiv,2)
	dspy2 = round(as.numeric(Eac2)/ratdiv,2)
	dataread = dataread2
  DA = format(round(nrow(dataread)/2.88,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 479,]
  tdx = tdx[tdx > 479]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(dataread2[,9]),]
  missingfactor = 108 - nrow(dataread2)
  dataread2 = dataread2[as.numeric(dataread2[,9]) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
		{
			if(METERCALLED == 1){
				TIMESTAMPSALARM <<- as.character(dataread2[,1])}
			else if(METERCALLED == 2){
		 	 TIMESTAMPSALARM2 <<- as.character(dataread2[,1])}
		}
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/1.2,1),nsmall=1)
  df = data.frame(Date = substr(as.character(dataread[1,1]),1,10), Eac1 = as.numeric(Eac1),Eac2 = as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,DailySpecYield1=dspy,DailySpecYield2=dspy2,LastRead = LR,LastTime = LT,stringsAsFactors=F)
  write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1, filepathm2,writefilepath)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t")
  dataread2 = read.table(filepathm2,header = T,sep = "\t")
  Eac1T = as.numeric(dataread1[,2]) + as.numeric(dataread2[,2])
  Eac2T = as.numeric(dataread1[,3]) + as.numeric(dataread2[,3])
  df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),EacTotMethod1Meter1 = as.numeric(dataread1[,2]),EacTotMethod1Meter2 = as.numeric(dataread2[,2]),EacTotMethod2Meter1=as.numeric(dataread1[,3]),EacTotMethod2Meter2=as.numeric(dataread2[,3]),EacSiteTotMethod1 = Eac1T,EacSiteTotMethod2 = Eac2T,LastRead=as.numeric(dataread1[,8]),LastRead2=as.numeric(dataread2[,8]),LastTime=dataread2[,9],stringsAsFactors=F)
  {
    if(file.exists(writefilepath))
    {
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
  return(c(as.numeric(Eac1T),as.numeric(Eac2T)))
}

