import requests, json
import requests.auth
import pandas as pd
import datetime as dt
import os
import re 
import time
import shutil
import pytz
import sys
import numpy as np
import logging
import pyodbc

pathread = '/home/admin/Dropbox/GIS_API3/'
pathwrite = '/home/manav/Dropbox/GIS_API3/'

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

sites = os.listdir(pathread)
sites.sort()
print(sites)

for i in sites:
  if os.path.isdir(pathread + i):
    for j in os.listdir(pathread + i + '/'):
      print('Opening File:' + j)
      df = pd.read_csv(pathread + i + '/' + j, sep = '\t')
      df['Date']= pd.to_datetime(df['Date'])
      chkdir(pathwrite + i)
      df.to_csv(pathwrite + i + '/' + j, sep='\t', index=False)
      print('Processed File:' + j)      