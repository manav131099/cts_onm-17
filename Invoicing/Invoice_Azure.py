import pandas as pd
import datetime
import pytz
import pyodbc
import os
from calendar import monthrange
import math



tz = pytz.timezone('Asia/Singapore')
path='/home/admin/Dropbox/HQDigest/Invoice/'
path_lt='/home/admin/Dropbox/Lifetime/Gen-1/'
path_tesco='/home/admin/Dropbox/Gen 1 Data/'
stations=['[TH-010L]', '[TH-011L]', '[TH-012L]', '[TH-013L]', '[TH-014L]', '[TH-015L]', '[TH-016L]', '[TH-017L]', '[TH-018L]', '[TH-019L]', '[TH-020L]', '[TH-021L]', '[TH-022L]', '[TH-023L]', '[TH-024L]', '[TH-025L]', '[TH-026L]', '[TH-027L]', '[TH-028L]']

def tesco(timenowdate):
    substations=[['MFM_1_PV Meter','MFM_2_Grid meter'], ['MFM_1_PV meter','MFM_2_Grid Meter'], ['MFM_2_PV Meter','MFM_1_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], [ 'MFM_2_PV Meter','MFM_1_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], [ 'MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], [ 'MFM_1_PV MFM-1','MFM_2_PV MFM-2','MFM_3_EB MFM'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], [ 'MFM_1_PV Meter','MFM_2_Grid Meter'], ['MFM_1_PV Meter','MFM_2_Grid MFM'], ['MFM_2_PV Meter','MFM_1_Grid MFM']]
    for j,i in enumerate(stations):
        temp=substations[j]
        print(i)
        for index,k in enumerate(temp):
            df=pd.read_csv(path_tesco+i+'/'+timenowdate[0:4]+'/'+timenowdate[0:7]+'/'+k+'/'+i+'-'+k[0:3]+k[4]+'-'+timenowdate[0:10]+'.txt',sep='\t')
            if(i[1:-2]!='TH-024'):
                if(index==0):
                    if(i[1:-2]=='TH-010' or i[1:-2]=='TH-017' or i[1:-2]=='TH-021' or i[1:-2]=='TH-022' or i[1:-2]=='TH-014'):
                        frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'C','LastTime': df.iloc[240,0],'LastRead': float(df.loc[240,'TotWhNet_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhNet_max'])/1000)} 
                    elif(i[1:-2]=='TH-016'):
                        frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'D','LastTime': df.iloc[240,0],'LastRead': float(df.loc[240,'TotWhNet_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhNet_max'])/1000)}
                    else:
                        frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'A','LastTime': df.iloc[240,0],'LastRead': float(df.loc[240,'TotWhNet_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhNet_max'])/1000)} 
                elif(index==1):
                    if(i[1:-2]=='TH-012' or i[1:-2]=='TH-015' or i[1:-2]=='TH-018'):
                        frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'C','LastTime': df.iloc[240,0],'LastRead': float(df.loc[240,'TotWhImp_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhImp_max'])/1000)} 
                    else:
                        frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'B','LastTime': df.iloc[240,0],'LastRead': float(df.loc[240,'TotWhImp_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhImp_max'])/1000)}
            else:
                if(index==0):
                    frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'A','LastTime': df.iloc[240,0],'LastRead': float(df.loc[240,'TotWhNet_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhNet_max'])/1000)} 
                elif(index==1):
                    frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'B','LastTime': df.iloc[240,0],'LastRead': float(df.loc[240,'TotWhNet_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhNet_max'])/1000)} 
                elif(index==2):
                    frame = { 'Operation': i[1:-2], 'MeterRef':i[1:-2]+'C','LastTime':df.iloc[240,0] ,'LastRead': float(df.loc[240,'TotWhImp_max'])/1000,'ReadingInv': round(float(df.loc[240,'TotWhImp_max'])/1000)} 
            result = pd.DataFrame(frame,columns=['Operation','MeterRef','LastTime','LastRead','ReadingInv'],index=[0])
            result.to_csv(path+'EOD_Meter_Reading_'+timenowdate+'.txt',sep='\t',index=False,mode='a',header=False) 

#Getting data from Azure
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

date=(datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')

connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
SQL_Query = pd.read_sql_query('SELECT TOP (1000) [Station_Id],[Station_Name],[GIS_Location] FROM [dbo].[stations] ', connStr)
df_stations = pd.DataFrame(SQL_Query, columns=['Station_Id','Station_Name','GIS_Location'])
SQL_Query = pd.read_sql_query('SELECT [Meter_Id],[Reference] FROM [dbo].[Meters] ', connStr)
df_meters = pd.DataFrame(SQL_Query, columns=['Meter_Id','Reference'])
SQL_Query = pd.read_sql_query("SELECT [Date],[Station_Id],[Meter_Id],[LastR-MFM] FROM [dbo].[Stations_Data] Where Date='"+date+"'", connStr)
df_stations_data = pd.DataFrame(SQL_Query, columns=['Date','Station_Id','Meter_Id','LastR-MFM'])

#Merging
df_merge=pd.merge(df_meters,df_stations_data,on='Meter_Id',how="left")
df_merge=pd.merge(df_merge,df_stations,on='Station_Id',how="left")

df_merge=df_merge[['Station_Name','Reference','LastR-MFM']]
df_merge['Station_Name']=df_merge['Station_Name'].str.strip()
df_merge['LastTime']=None

#Adding Timestamp
for index,row in df_merge.iterrows():
    try:
        df_temp=pd.read_csv(path_lt+row['Station_Name']+'-LT.txt',sep='\t')
        last_time=df_temp.loc[df_temp['Date']==date,'LastT']
        df_merge.loc[index,'LastTime']=last_time.values[0]
    except:
        pass

df_merge['ReadingInv'] = df_merge['LastR-MFM'].apply(lambda x: round(x, 0))

df_merge.columns=['Operation','MeterRef','LastRead','LastTime','ReadingInv']
df_merge=df_merge[['Operation','MeterRef','LastTime','LastRead','ReadingInv']]

df_merge=df_merge.dropna(subset=['Operation'])
df_merge=df_merge.fillna('NA')
df_merge=df_merge.replace({0:'NA'})

for i in stations:
    df_merge = df_merge[df_merge['Operation'] != i[1:-2]]
df_merge.to_csv(path+'EOD_Meter_Reading_'+date+'.txt',sep='\t',index=False)
tesco(date)