# load the required packages
library('shiny')
require('shinydashboard')
library('shinyjs')
#library(ggplot2)
#library(dplyr)

#head(data tables)
maillist <- read.csv('/home/saradindu/MEGA/Cleantech_Solar/testbed/Dashboard/mail_recipients.csv',stringsAsFactors = F,header=T)
alertlist<- read.csv('/home/saradindu/MEGA/Cleantech_Solar/testbed/Dashboard/alerts_recipients.csv',stringsAsFactors = F,header=T)


GetTableMetadata <- function()
{
  fields <- c(recipient = "Recipient",
              mail="E-mail ID",
              station = "Station",
              yn = "Yes or No")

  result <- list(fields = fields)
  return (result)
}

GetNextId <- function()
{
  if (exists("responses") && nrow(responses) > 0)
  {
    as.integer(nrow(responses)) + 1
  }
  else
  {
    return (1)
  }
}

CreateData <- function(data)
{

  data <- CastData(data)
  rownames(data) <- GetNextId()
  if (exists("responses")) {
    responses <<- rbind(responses, data)
  } else {
    responses <<- data
  }
}

ReadData <- function()
{
  if (exists("responses")) {
    responses
  }
}

UpdateData <- function(data)
{
  data <- CastData(data)
  responses[row.names(responses) == row.names(data), ] <<- data
}

CastData <- function(data)
{


  datar <- data.frame(recipient = data["Recipient"],
                      station = as.character(data["station"]),
                      yn = as.integer(data["yn"]),
                      stringsAsFactors = FALSE)

  return (datar)
}

CreateDefaultRecord <- function()
{
  mydefault <- CastData(list(id = "0", name = "", used_shiny = FALSE, r_num_years = 2))
  return (mydefault)
}

UpdateInputs <- function(data, session)
{

  updateTextInput(session, "recipient", value = unname(data["name"]))
  updateTextInput(session, "mail", value = unname(data["mail"]))
  updateSelectInput(session, "station", "Station",choices = colnames(maillist))
  updateSelectInput(session, "yn","Yes or No",choices = c('Yes','No'))
}

defineStation <- function(data, type)
{
  df=as.data.frame(data)
  names=colnames(df)
  if(type== "m")
  {
    nam1=names[2:ncol(df)-2]
    return(nam1)
  }
  if(type== "a")
  {
    nam1=names[3:ncol(df)-2]
    return(nam1)
  }
}

#Dashboard header carrying the title of the dashboard
header <- dashboardHeader(title = "Mail and Alert")

#Sidebar content of the dashboard
sidebar <- dashboardSidebar(
  sidebarMenu(
    menuItem("Mail & Alerts", tabName = "mail&Alerts", icon = icon("dashboard"),
             menuSubItem('Overview',
                         tabName = 'overview',
                         icon = icon('dashboard')),
             menuSubItem('Modify Recipient- Mail',
                         tabName = 'modify',
                         icon = icon('dashboard')),
             menuSubItem('Modify Recipient- SMS',
                         tabName = 'modify1',
                         icon = icon('dashboard'))
             ),
    menuItem("Visit-us", icon = icon("send",lib='glyphicon'),
             href = "https://www.cleantechsolar.com")
  )
)
body <- dashboardBody(
tabItems(
tabItem(tabName = "overview",
fluidRow(
  valueBoxOutput("value1")
  ,valueBoxOutput("value2"),
  valueBoxOutput("value3")
),

fluidRow(

  box(
    title = "Digest List"
    ,status = "primary"
    ,solidHeader = TRUE
    ,collapsible = TRUE
    ,dataTableOutput("digest")
  )

  ,box(
    title = "Alert List"
    ,status = "primary"
    ,solidHeader = TRUE
    ,collapsible = TRUE
    ,dataTableOutput("alert")
  )
)
),
tabItem(tabName = "modify",useShinyjs(),

        div(style = 'overflow-x: scroll',DT::dataTableOutput("response")),
        tags$hr(),
        textInput("recipient", "Recipient", ""),
        textInput("mail", "E-mail ID", ""),
        selectInput("station", "Station",choices = colnames(maillist),multiple = TRUE),
        selectInput("yn", "Yes or No",choices = c('Yes','No')),

        #action buttons
        actionButton("submit", "Submit"),
        actionButton("new", "New"),
        actionButton("delete", "Delete")
     ),
tabItem(tabName = "modify1",useShinyjs(),

        div(style = 'overflow-x: scroll',DT::dataTableOutput("response1")),
        tags$hr(),
        textInput("recipient", "Recipient", ""),
        textInput("phone", "Phone", ""),
        selectInput("station", "Station",choices = colnames(alertlist),multiple = TRUE),
        selectInput("yn", "Yes or No",choices = c('Yes','No')),

        #action buttons
        actionButton("submit", "Submit"),
        actionButton("new", "New"),
        actionButton("delete", "Delete")
)
)
)



#completing the ui part with dashboardPage
ui <- dashboardPage(title = 'Mail and Alert', header, sidebar, body, skin='red')

# create the server functions for the dashboard
server <- function(input, output, session) {

  #some data manipulation to derive the values of KPI boxes
  #maillist <- read.csv('/home/saradindu/MEGA/Cleantech_Solar/testbed/Dashboard/mail_recipients.csv',stringsAsFactors = F,header=T)
  #alertlist<- read.csv('/home/saradindu/MEGA/Cleantech_Solar/testbed/Dashboard/alerts_recipients.csv',stringsAsFactors = F,header=T)
  mailname<-colnames(maillist)
  Names=maillist[,1]
  names1=alertlist[,1:2]
  #creating the valueBoxOutput content
  output$value1 <- renderValueBox({
    valueBox(
      formatC(length(Names), format="d", big.mark=',')
      ,'No of Recipients for Digest'
      ,icon = icon("pushpin",lib='glyphicon')
      ,color = "purple")


  })

  output$value2 <- renderValueBox({

    valueBox(
      formatC(nrow(names1), format="d", big.mark=',')
      ,'No of Recipients for Alert'
      ,icon = icon("pushpin",lib='glyphicon')
      ,color = "green")

  })

  #creating the box content

  output$digest <- renderDataTable({

    as.data.frame(Names)

  })

  output$alert <- renderDataTable({

    as.data.frame(names1)
  })
  output$value3 <- renderValueBox({

    valueBox(
      formatC(32837.65, format="d", big.mark=',')
      ,'Total Capacity in India'
      ,icon = icon("pushpin",lib='glyphicon')
      ,color = "green")
  })

  output$response <- DT::renderDataTable(maillist)
  output$response1 <- DT::renderDataTable(alertlist)

}


#runApp('MEGA/Cleantech_Solar/testbed/Dashboard',host="0.0.0.0",port=5050)
shinyApp(ui, server)

