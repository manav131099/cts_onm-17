require('RCurl')
require('utils')
require('R.utils')
source('/home/admin/CODE/MasterMail/timestamp.R')
FIREMAIL = 0
retryCnt = 0
serverdownmail = function()
{
	try(send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "SG-001X FTP server down",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F),silent=T)
}
serverupmail = function()
{
	try(send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "SG-001X FTP server up and running",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F),silent=T)
}
dumpftp = function(days,path)
{
 	print('Enter function')
  url = "ftp://cleantech_sg:bscxataB2@ftpnew.cleantechsolar.com/Meter-2/"
  filenames = try(evalWithTimeout(getURLContent(url,ftp.use.epsv = FALSE,dirlistonly = TRUE,timeout = 600,ssl.verifypeer = FALSE,useragent="R"),
	 timeout = 600, onTimeout = "error"),silent=T)
	print('try success')
	if(class(filenames) == 'try-error')
	{
		print('Error in FTP server, will reconnect in 10 mins')
		retryCnt <<- retryCnt + 1
		if(FIREMAIL==0 && retryCnt > 3)
		{
			serverdownmail()
			FIREMAIL<<-1
		}
		return(0)
	}
	retryCnt <<-0
	if(FIREMAIL==1)
	{
	  print('Server up and running...')
		serverupmail()
		FIREMAIL<<-0
	}
	if(class(filenames)!='character')
	{
		print('Cant split filename as not character')
		return(0)
	}
	recordTimeMaster("SG-001X","FTPProbe")
	print('Filenames obtained')
  newfilenames = unlist(strsplit(filenames,"\n"))
	print('Filenames split')
  newfilenames = newfilenames[grepl("zip",newfilenames)]
	tmps = newfilenames[grepl("tmp",newfilenames)]
	print(paste('length of tmps',tmps))
	print(paste('newfilenames length before delete',length(newfilenames)))
	if(length(tmps) > 1)
	{
		idxmtchrm = match(tmps,newfilenames)
		newfilenames = newfilenames[-idxmtchrm]
	}
	print(paste('newfilenames length after delete',length(newfilenames)))
	print('Zips found')
	newfilenamesb = newfilenames[grepl("Tmod",newfilenames)]
	newfilenamesa = newfilenames[grepl("Gsi00",newfilenames)]
	newfilenames = newfilenames[grepl("PowerMeter1_",newfilenames)]
	newfilenames = c(newfilenames,newfilenamesa,newfilenamesb)
	days2 = unlist(strsplit(days,"\\."))
	seq1 = seq(from = 1,to = length(days2),by=2)
	days2 = days2[seq1]
	newfilenames2 = unlist(strsplit(newfilenames,"\\."))
	newfilenames2 = newfilenames2[seq1]
#	tail(newfilenames2)
#	head(newfilenames2)
#	tail(days2)
#	head(days2)
	match = match(days2,newfilenames2)
	print(paste('Length of match',length(match)))
	match = match[complete.cases(match)]
	if(length(match) < 1)
	{
	  print('No new files')
		return(1)
	}
  newfilenames = newfilenames[-match]
	if(length(newfilenames) < 1)
	{
	  print('No new files')
		return(1)
	}
	for(y in 1 : length(newfilenames))
  {
    urlnew = paste(url,newfilenames[y],sep="")
    file = try(download.file(urlnew,destfile = paste(path,newfilenames[y],sep="/")),silent = T)
		if(class(file) == 'try-error')
		{
			print(paste('couldnt download',newfilenames[y]))
			next
		}
	  unzipping = try(unzip(paste(path,newfilenames[y],sep="/"),exdir = path),silent = T)
		if(class(unzipping) == 'try-error')
		{
			print(paste('Couldnt extract',newfilenames[y]))
		}
		system(paste('rm "',path,'/',newfilenames[y],'"',sep = ""))
  }
	recordTimeMaster("SG-001X","FTPNewFiles",as.character(newfilenames[length(newfilenames)]))
	return(1)
}
